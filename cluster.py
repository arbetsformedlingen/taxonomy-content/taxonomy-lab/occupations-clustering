import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import utils as u
from collections import defaultdict
from itertools import product
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
import sklearn.metrics as metrics
from data_utils import get_skills_freq

class Cluster:
    def __init__(self,
                 skills_and_occupations, 
                 metric,
                 skill_type, 
                 n_skill_offset=0,
                 traits=None, 
                 occupations_count=None):
        """
        Args:
            metric(str): 
                accepted values are 'cosine' and 'euclidean'

            skills_and_occupations (str): 
                path to skill pickle file in init_files dir, 
                contains dict of counted pairs of occupations and skills

            skill_type (str): 
                accepted values are 'trait', 'skill' and 'all'

            traits (str): 
                pickle file containing set of traits found in ads, 
                will use 'init_files/all_traits.pickle' if not specified

            occupations_count (str): 
                pickle file containing a counter with each occupation (id) found in the ads dataset,
                set to 'init_files/occupations_count.pickle' if not specified

            n_skill_offset (int): 
                specifies the offset for skills in descending order (more popular skills comes first in the list),
                skills within the offset will be excluded. Set to 0 by default. Mostly used for testing purposes. 
                I think this one shoul
        """
        self.metric = metric
        self.occupation_id_legacy_mappings = u.load_json("resources/occupation_ids_and_legacy.json")
        self.skills_and_occupations = u.load_pickle(skills_and_occupations)
        self.skill_type = skill_type
        self.occupation_id_label_mappings = u.occupation_id_mappings("resources/occupation_id_mappings.csv")
        self.traits = u.load_pickle("init_files/all_traits.pickle") if traits is None else u.load_pickle(traits) 
        self.filter_skills = u.read_csv("resources/skills_filter.csv")[0]
        self.skill_offset = list(get_skills_freq(self.skills_and_occupations))[:n_skill_offset] 
        self.occupations_count = u.load_pickle("init_files/occupations_count.pickle") if occupations_count is None else u.load_pickle(occupations_count)
        self.occupations_and_skills_counted = self.occupations_with_skill_counts(self.skills_and_occupations)
        self.intensities = self.group_occupations_with_skill_intensities(self.occupations_and_skills_counted, self.skill_type)
        self.feature_vectors = self.construct_feature_vectors(self.intensities)
        self.distance_matrix = self.construct_distance_matrix(self.metric, self.feature_vectors)
        self.labels = [v for v, _ in self.occupations_count.items()]
        self.distance_matrix_transformed = self.reduce_dimensions(self.distance_matrix)
        self.feature_matrix_transformed = self.reduce_dimensions(self.feature_vectors)

        matplotlib.use("tkagg")

    def occupations_filter(self, filepath):
        return [id for id, _ in u.load_pickle(filepath)]

    def occupations_with_skill_counts(self, skills_and_occupations):
        """
        Returns dict in which occupations are keys instead of skills. 
        Each occupation will have a subdict with skills as keys and 
        intensities as values.
        """
        return u.flip_dict(skills_and_occupations)
        
    def create_occupation_pairs(self, m):
        """
        Produces list of every occupation pair. Used for distance calculations-

        Args: 
            m (coll): most likely a dict where each occupation occurs as a key.
        
        """
        return list(product(m, repeat=2))

    def check_skills(self, skill, skill_type):
        """
        Checks if skill is in filter list and returns it or None
        based on skill_type. 

        Args:   
            skill (str): 
                skill to be checked

            skill_type: 
                either 'skill', 'trait' or 'all'
        
        """
        if skill not in self.filter_skills and skill not in self.skill_offset:
            if skill_type == "skill":
                return skill if skill not in self.traits else None
            elif skill_type == "trait":
                return skill if skill in self.traits else None
            elif skill_type == "all":
                return skill

    def merge_dicts(self, occupation_ids_and_skills):
        """
        Puts multiple dicts (of dicts) in a collection to one dict, 
        subdicts merged.

        Args:
            occupation_ids_and_skills (list):
                list of dicts to be merged
        """ 
        merged_dicts = defaultdict(dict)
        for item in occupation_ids_and_skills:
            for occupation_id, skills in item.items():
                merged_dicts[occupation_id].update(skills)
        return merged_dicts

    def group_occupations_with_skill_intensities(self, occupations_and_skills, skill_type):
        """
        Args:
            occupations_and_skills (dict): 
                dict where key=occupation_id and val=dict 
                with count for each skill associated to the occupation. 
            
            skill_type (str):  
                accepted values are 'skill', 'trait' and 'all'

        Skill intensity = number of times a skill is seen together with an occupation
        divided by how many times the occupation is seen in total in the dataset.
        """
        ids_and_skills = []
        for occupation, v in occupations_and_skills.items():
            for skill, count in v.items():
                skills_percentage = round(count/int(self.occupations_count[occupation]), 5)
                skill_with_intensity = self.check_skills(skill, skill_type)
                if skill_with_intensity != None:
                    ids_and_skills.append({occupation: {skill_with_intensity: skills_percentage}})
        # create map with all occupation-ids and corresponding 
        # skill and intensity combo as dict
        return self.merge_dicts(ids_and_skills)

    def construct_feature_vectors(self, intensities):
        """
        Builds vectors for occupations where each position in a vector represents
        the occupation's skill intensity.

        Args:
            intensities (dict):
                occupation-ids (as keys) and their associated skills
                and skill intensities (as values)

        """
        skills = list(self.skills_and_occupations)
        occupations = [x for x,_ in self.occupations_count.items()]
        intensities_flipped = u.flip_dict(intensities)
        matrix = []
        for occupation in occupations:
            feature_vector = []
            for skill in skills:
                occ = intensities_flipped[skill]
                skill_intensity = occ.get(occupation, 0)
                feature_vector.append(skill_intensity)
            matrix.append(feature_vector)
        return np.array(matrix)
    
    def map_occupations_to_feature_vectors(self, vectors, occupations):
        """
        Maps each feature vector to its occupation.
        Returns as dict. Can be used to more easily compare
        feature vectors of two specific occupations.
        
        Args:
            vectors (nparray): 
                array of feature vectors
            
            occupations (dict):
                
        """
        return {occupations[i]: vectors[i] for i in range(len(occupations))}


    def construct_distance_matrix(self, metric, vectors):
        """
        Calculates pairwise relations for each occupation. 

        Args:
            metric (str):
                accepts 'euclidean' or 'cosine'

            vectors (nparray):
                feature vectors representing occupations, each feature a skill
        """
        if metric == "euclidean":
            return metrics.pairwise.euclidean_distances(vectors)

        elif metric == "cosine":
            return metrics.pairwise.cosine_distances(vectors)


    def plot_data(self, transformed_matrix, labels):
        """
        Tries to plot the data in 2d. 
        The matrix will have to be reduced before plotting.

        Args:
            transformed_matrix (nparray):
                matrix that has been dimensionally reduced (i.e. through PCA or MDS)

            labels (list):
                list of all labels

        """
        x,y = transformed_matrix.T
        fig, ax = plt.subplots()
        ax.scatter(x,y)
        for i, txt in enumerate(labels):
            ax.annotate(self.occupation_id_label_mappings[txt], (x[i], y[i]))
        plt.show()

    def reduce_dimensions(self, matrix):
        """
        The dimension reduction technique that seems to have generated 
        best results is the PCA, at least intuitively, therefore used here. 

        Args:
            matrix (nparray):
                distance matrix or feature vectors
        """
        embedding = PCA(n_components=2)
        return embedding.fit_transform(matrix)

    def cluster(self, matrix):
        # Note that a distance matrix should not be used as input to kmeans, pass feature_vectors instead
        labels = self.labels
        cluster = KMeans(n_clusters=3).fit(matrix)
        return zip(cluster.labels_, labels)



cluster = Cluster(skills_and_occupations="init_files/skills_count_sample.pickle", 
                  occupations_count = "init_files/occupations_count_sample.pickle",
                  traits = "init_files/all_traits_sample.pickle",
                  metric="cosine",
                  skill_type="skill",
                  n_skill_offset=0)


