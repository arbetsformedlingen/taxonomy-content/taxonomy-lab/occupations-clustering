import json
import csv
import pickle 
from collections import defaultdict
import numpy as np
from math import isnan
from numpy.linalg import norm

def write_to_csv(filepath, data):
    with open(filepath, 'w') as file:
        writer = csv.writer(file)
        writer.writerow(data)

def load_jsonl(path):
    data=[]
    with open(path, 'r', encoding='utf-8') as reader:
        data.extend(json.loads(line) for line in reader)
    return data 

def load_json(path):
    data = []
    with open(path, 'r', encoding="utf-8") as reader:
        return json.load(reader)
        

def write_dict_to_csv(filepath, data):
    with open(filepath, 'w') as f: 
        writer = csv.writer(f)
        for key, value in data.items():
            writer.writerow([key, value])

def read_csv(filepath):
    result = []
    with open(filepath, 'r', encoding='utf-8-sig') as file:
        reader = csv.reader(file)
        result.extend(iter(reader))
    return result
    
def occupation_id_mappings(path):
    return dict(read_csv(path))
    
def save_pickle(filepath, obj):
    """
    Save specified obj as pickle to local repository.

    """
    with open(filepath, "wb") as f: 
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_pickle(filepath):
    """
    Load pickle file of specified name from local repository.

    """
    with open(filepath, "rb") as f: 
        return pickle.load(f)


def flip_dict(data):
    "Flips dict with one nested dict layer."
    intensities_flipped = defaultdict(dict)
    for key, val in data.items():
        for subkey, subval in val.items():
            if subkey != "Total":
                intensities_flipped[subkey][key] = subval
    return intensities_flipped


def calculate_cos_similarity(feat_vector_x, feat_vector_y):
        """
        Args: 
            feat_vector_x (nparray): 
                feature vector from occupation x
            feat_vector_y (nparray):
                feature vector from occupation y
        """
        cosine = np.dot(feat_vector_x,feat_vector_y)/(norm(feat_vector_x)*norm(feat_vector_y))
        # If no pairs of x and y coincide we do not want them computed
        # since it will only generate nan-values, in these cases they should have 0 similarity
        return 0.0 if isnan(cosine) else cosine
            
def calculate_eucl_distance(feat_vector_x, feat_vector_y):
    """
    Args: 
        feat_vector_x (nparray): 
            feature vector from occupation x
        feat_vector_y (nparray):
            feature vector from occupation y
    """
    return norm(feat_vector_x - feat_vector_y)