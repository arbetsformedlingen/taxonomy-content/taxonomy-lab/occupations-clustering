import utils as u
import os
from collections import OrderedDict
from operator import itemgetter

def get_ads(path, limit=None):
    return u.load_jsonl(path)[:limit]

def get_all_ads():
    """
    Do not run this fn if there are many files in the ads folder. 
    Will consume a LOT of memory. Mostly used for testing.
    """
    result = []
    paths = os.listdir("data/ads")
    for path in paths:
        ads = u.load_jsonl(f"data/ads/{path}")
        result.extend(iter(ads))
    return result
    
def get_skills_freq(data):
    """
    Feed with skills_count pickle
    """
    data_reduced = {x:y["Total"] for x,y in data.items()}
    return OrderedDict(sorted(data_reduced.items(), key=itemgetter(1), reverse=True))


def get_skill_freq_by_skill(skill, data):
    return get_skills_freq(data)[skill]

def get_skill_with_occupations(skill, data):
    return data[skill]


def get_intensities(id, intensities):
    """
    Get skill intensities for specified occupation.
    """
    return sorted(intensities[id].items(), key=lambda x: x[1], reverse=True)