import utils as u
from collections import Counter, defaultdict 
import os


"""
Check out the Readme on how to gather new data. 

"""


def legacy_id_mappings(path):
    """
    Old ads do not have concept-ids for occupation concepts. 
    This map will translate from legacy-id to concept-id.
    """
    data = u.load_json(path)["data"]["concepts"]
    return {m["deprecated_legacy_id"]: m["id"] for m in data}

def count_ads(ads, specify_occupations=None, cutoff=50):
    """
    Args: 
        cutoff: determines how often a particular occupation
        needs to be present in the dataset to be included in the count. 
        Arbitrarily set to 50 as default.

        Specify occupations should be a list of occupation-ids. If specified, 
        only ads with occupations found in the list will be gathered.
    """
    c = Counter()
    legacy_ids = legacy_id_mappings("resources/occupation_ids_and_legacy.json")
    for ad in ads:  
        legacy_id = ad["occupation"][0]["legacy_ams_taxonomy_id"]
        # some empty values seem to be indicated with an empty string in the ads_file, some with 999999, 
        # some old values pre version 1, so:
        if legacy_id in legacy_ids.keys():
            concept_id = legacy_ids[legacy_id]
            c[concept_id] += 1
    if specify_occupations:
        return {k: v for k, v in c.items() if k in specify_occupations and v > cutoff}
    else:
        return dict(filter(lambda item: item[1] > cutoff, c.items()))
    

def create_occupation_filter_file_pickle(ads, path_out, specify_occupations=None, cutoff=50):
    """
    Create pickle where every vector shows an occupation-name id and its count
    throughout the input data.

    Use this function first to process one ad-file at the time. 

    The files will then be merged.

    Going through all files at once is likely too memory consuming.

    """
    u.save_pickle(path_out, count_ads(ads=ads, cutoff=cutoff, specify_occupations=specify_occupations))


def merge_trait_files(path_out, dir_trait_files):
    """
    Merges all files (.pickle) in the specified directory. 
    Accepted files are created through functions create_occupation_count_year_file 
    and create_skills_count_year_file.

    Basically a dict-merge for the dicts stored in the files.
    Creates an aggregate of these files which will be used to initialize the 
    clustering.

    Args: 
        path_out (str): file should preferably be stored in init_files
        dir_trait_files (str): generally trait_files_by_year 
    
    """
    files = os.listdir(dir_trait_files)
    result_traits = set()
    for file in files:
        contents = u.load_pickle(f"{dir_trait_files}/{file}")
        for trait in contents:
            result_traits.add(trait)
    u.save_pickle(path_out, result_traits)

def merge_occupation_files(path_out, dir_occupation_files):
    """
    Merges all files (.pickle) in the specified directory. 
    Accepted files are created through functions create_occupation_count_year_file 
    and create_skills_count_year_file.

    Basically a dict-merge for the dicts stored in the files.
    Creates an aggregate of these files which will be used to create the skill_init_file

    Args: 
        path_out (str): file should preferably be stored in resources
        dir_occupation_files (str): generally occupation_files_by_year 
    """
    files = os.listdir(dir_occupation_files)
    result_occupations = Counter()
    for file in files:
        contents = u.load_pickle(f"{dir_occupation_files}/{file}")
        for k,v in contents.items():
            if k != None:
                result_occupations[k] += v
    u.save_pickle(path_out, result_occupations)

def merge_skill_files(path_out, dir_skill_files):
    """
    Merges all files (.pickle) in the specified directory. 
    Accepted files are created through functions create_occupation_count_year_file 
    and create_skills_count_year_file.

    Basically a dict-merge for the dicts stored in the files.
    Creates an aggregate of these files which will be used to initialize 
    the clustering.

    Args: 
        path_out (str): file should preferably be stored in init_files
        dir_skill_files (str): generally skill_files_by_year 
    """
    files = os.listdir(dir_skill_files)
    result_skills = {}
    for file in files:
        contents = u.load_pickle(f"{dir_skill_files}/{file}")
        for skill,skill_dict in contents.items():
            if skill in result_skills:
                for ks, _ in result_skills[skill].items():
                    for s, c in skill_dict.items():
                        if s in result_skills[skill].keys():
                            result_skills[skill][s] += c
                        else:
                            x = result_skills[skill].copy()
                            x[ks] = c
                            result_skills[skill] = x
            else:
                result_skills[skill] = skill_dict
    u.save_pickle(path_out, result_skills)  

def create_skills_stats(ads, path_occupations, path_filter_skills, path_out_all_skills, path_out_traits):
    #TODO Warning: a big mess - simplify
    """
    This file will act as a basis for later functions concerning skills.
    """
    result = defaultdict(dict)
    # traits written to file in order to make filtering possible later on
    all_traits = []
    filter_skills = u.read_csv(path_filter_skills)[0]
    occupations = [id for id,count in u.load_pickle(path_occupations).items()]
    legacy_ids = legacy_id_mappings("resources/occupation_ids_and_legacy.json")
    for ad in ads:
        legacy_id = ad["occupation"][0]["legacy_ams_taxonomy_id"]
        if legacy_id in legacy_ids.keys():
            concept_id = legacy_ids[legacy_id]
            if concept_id in occupations:
                # enriched attrib only found in every 100th or so ad
                if "enriched" in ad["keywords"].keys(): 
                    traits = ad["keywords"]["enriched"]["trait"]
                    skills = ad["keywords"]["extracted"]["skill"] + ad["keywords"]["enriched"]["skill"] + traits 
                    all_traits.extend(iter(traits))
                else:
                    skills = ad["keywords"]["extracted"]["skill"]
                for skill in skills:
                    if skill not in filter_skills:
                        if skill in result:
                            result[skill]["Total"] += 1
                        else:
                            result[skill]["Total"] = 1
                        if concept_id not in result[skill].keys():
                            result[skill][concept_id] = 1
                        else:
                            result[skill][concept_id] += 1
    u.save_pickle(path_out_traits, set(all_traits))
    u.save_pickle(path_out_all_skills, result)

def create_occupation_count_year_file(year, specify_occupations=None):
    """
    Specify year (str) and file with each occupation counted for that year's ads will be created.

    Args:
        year (str): 
            year (from 2016-2022_2) - note that 2022 contains more than 1 file
            
        specify_occupations (list): 
            concept-ids for occupations to be harvested.
            None means all avaliable occupations will be considered
    
    """
    create_occupation_filter_file_pickle(ads=u.load_jsonl(f"data/ads/{year}.metadata.jsonl"), 
                                         path_out=f"occupation_files_by_year/occupations_count_{year}.pickle", 
                                         specify_occupations=specify_occupations)

def create_skill_count_year_file(year):
    """
    Specify year (str) and file with each skill-occupation pair counted for that year's ads will be created.
    The merged occupation file needs to be created before running this.

    Args:
        year (str): year (from 2016-2022_2) - note that 2022 contains more than 1 file
    """
    create_skills_stats(ads=u.load_jsonl(f"data/ads/{year}.metadata.jsonl"), 
                        path_occupations="init_files/occupations_count.pickle", 
                        path_filter_skills="resources/skills_filter.csv", 
                        path_out_all_skills=f"skill_files_by_year/skills_count_{year}.pickle", 
                        path_out_traits=f"trait_files_by_year/traits_{year}.pickle")


