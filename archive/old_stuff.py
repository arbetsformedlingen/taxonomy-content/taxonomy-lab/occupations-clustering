
'''

def amend_differences(x, y, multiplier=None):
    """
    Harmonize two dicts. If keys not equal in the dicts,
    add the missing keys to both dicts with _ as value. 
    """
    x_amended = {}
    y_amended = {}
    x_keys = set(x.keys())
    y_keys = set(y.keys())
    diff_x = x_keys.difference(y_keys)
    diff_y = y_keys.difference(x_keys)
    if diff_x:
        for skill in diff_x:
            #print(x[skill])
            y_amended[skill] = 0.0  
    if diff_y:
        for skill in diff_y:
            x_amended[skill] = 0.0
    x_amended |= x
    y_amended |= y
    return x_amended, y_amended
    

def map_occupations_to_distance_vectors(self, distance_map):
    #TODO gör inte denna samma sak som feature_vectors_mapped?
    #TODO denna behöver användas för att skapa distance-matrix
    # distance map eller similarity map skickas in här
    """
    Squared distance of skill intensities: ...
    Dissimilarity score: the higher the value the less similar a pair is

    Returns defaultdict where each key is an occupation 
    and each value a vector of distances.
    """
    result = defaultdict(list)
    occupation_pairs = self.create_occupation_pairs(distance_map)
    for occupation_x, occupation_y in occupation_pairs:
        skills_x, skills_y = (distance_map[occupation_x], distance_map[occupation_y])
        distance = self.get_distance(self.metric, skills_x, skills_y)
        result[occupation_x].append(distance)
    return result

def get_distance(self, metric, m):
    """
    Get distance measurement between two vectors based on supplied metric.

    Args: 
        skills_x (list): 
            feature vector from occupation x
        skills_y (list):
            feature vector from occupation y

    """
    if metric == "euclidean":
        return metrics.pairwise.euclidean_distances(m)
        #return self.calculate_eucl_distance(skills_x, skills_y)
    elif metric == "cosine":
        return metrics.pairwise.cosine_distances(m)
        #return self.calculate_cos_similarity(skills_x, skills_y)


def construct_distance_matrix(self, occupations_and_distances):
    #TODO perhaps not necessary since sklearn's pairwise methods gets us the same result?
    #TODO dict should not be used here since order of occupations is important
    """
    Builds a symmetric distance matrix for the occupations. 

    Args:
        occupations_and_distances:
            ??? takes distance vectors ???
        
    """
    occupation_labels = list(occupations_and_distances)
    distance_vectors = [v for _, v in occupations_and_distances.items()]
    return occupation_labels, np.array(distance_vectors)

def boost_occupation_affinity(skills_x, skills_y, multiplier=0.5):
    #TODO Detta behöver ske innan skills_amended
  
    skills_count = u.load_pickle("resources/skills_count.pickle")
    skills_x_set = set(skills_x)
    skills_y_set = set(skills_y)
    if skills_intersect := skills_x_set.intersection(skills_y_set):
        for skill in skills_intersect:
            n_skill = skills_count[skill]["Total"]
            skills_x[skill]=skills_x[skill]+skills_y[skill]/2
            skills_y[skill]=skills_x[skill]+skills_y[skill]/2
            print(skills_x)

            print(skills_y)

 def calculate_cos_similarity(self, feat_vector_x, feat_vector_y):
        """
        Args: 
            skills_x (list): 
                feature vector from occupation x
            skills_y (list):
                feature vector from occupation y
        """
        # TODO Maybe redudant since sklearn has pairwise distance functions
        cosine = np.dot(feat_vector_x,feat_vector_y)/(norm(feat_vector_x)*norm(feat_vector_y))
        # If no pairs of x and y coincide we do not want them computed
        # since it will only generate nan-values, in these cases they should have 0 similarity
        return 0.0 if isnan(cosine) else cosine
               
    def calculate_eucl_distance(self, skills_x, skills_y):
        # TODO Maybe redudant since sklearn has pairwise distance functions
        """
        Args: 
            skills_x (list): 
                feature vector from occupation x
            skills_y (list):
                feature vector from occupation y
        """
        return norm(skills_x - skills_y)

    def get_distance_fya(self, skills_x, skills_y):
        """

        Args: 
            skills_x (list): 
                feature vector from occupation x
            skills_y (list):
                feature vector from occupation y
        
        """
        #TODO Maybe redudant since sklearn has pairwise distance functions
        #TODO test if this gives same results as eucl
        result = [(x-y)**2 for x, y in zip(skills_x, skills_y)]
        print(result)
        return sqrt(sum(result))

def save_object(path_filter, path_ads, path_pickle):
    """
    Use this to save occupation intensity map for speedy access.
    """
    occ_filter = occupations_filter(path_filter)
    ads = u.get_ads(path_ads)
    intensities = group_occupations_with_skill_intensities(pair_skills_with_occupations(occ_filter, ads))
    u.save_pickle(path_pickle, intensities)

def pair_skills_with_occupations(occupations_filter, ads):
    #TODO gör om denna, annonser behöver ej laddas in på nytt
    # Ta bort funktionen?
    #TODO slå av/på skills filter?
    """
    occupations_filter: vector of occupations to be used to build matrix.
    """
    occupations_and_skills = create_empty_dict(occupations_filter) # behövs ej
    filter_skills = skills_filter("resources/skills_filter.csv") 
    for ad in ads: # behövs ej
        occupation_id = ad["occupation"][0]["concept_id"]
       # enriched_occupations = ad["keywords"]["extracted"]["occupation"]
        enriched_skills = ad["keywords"]["extracted"]["skill"]
        for skill in enriched_skills: #behövs ej
            if skill not in filter_skills and occupation_id in occupations_filter:
                occupations_and_skills[occupation_id].update([skill])
    # outputen här är en dict med {concept-id: {skill1: n1
    #                                           skill2: n2}} etc
    return occupations_and_skills
'''